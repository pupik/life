var app = angular.module('app', ['ui.grid','ui.grid.pagination']);

app.controller('LifeController', function($scope, $http, $interval) {

    // initial values
    var numRows = 20;
    var numCols = 20;
    var animationPromise;
    $scope.animating = false;
    $scope.displayNumRows = numRows;
    $scope.displayNumCols = numCols;

    // initialize new grid
    $scope.initGrid = function(newNumRows, newNumCols) {
        if($scope.animating) {
            $scope.stopAnimation();
        }
        var emptyRow = new Array(newNumCols).fill(false);
        $scope.rows = new Array(newNumRows).fill(emptyRow);
        numRows = newNumRows;
        numCols = newNumCols;
    }

    $scope.initGrid(numRows, numCols);

    // click handler function to change cell state
    $scope.setCell = function(isAlive, colIndex, rowIndex) {
        var row = angular.copy($scope.rows[rowIndex]);
        row[colIndex] = isAlive;
        $scope.rows[rowIndex] = row;
    }

    // send post to servlet and progress grid to next round
    $scope.nextRound = function() {
        $http({
            method : 'POST',
            url : 'life/process',
            data : {
                numRows: numRows,
                numCols: numCols,
                rows: $scope.rows
            },
            headers: {
                'Content-Type': 'application/json'
            }
        }).success(function(data) {
            $scope.rows = data.rows;
        }).error(function(data) {
            console.log('error',data);
        });
    }

    // start animation - sets interval for post every 500ms
    $scope.startAnimation = function() {
        animationPromise = $interval(function() {
            $scope.nextRound();
            $scope.animating = true;
        }, 500);
    }

    // stop animation - cancels post interval
    $scope.stopAnimation = function() {
        $interval.cancel(animationPromise);
        $scope.animating = false;
    }

    // create glider in static place
    $scope.createGlider = function() {
        $scope.setCell(true,2,2);
        $scope.setCell(true,2,3);
        $scope.setCell(true,2,4);
        $scope.setCell(true,1,4);
        $scope.setCell(true,0,3);
    }
});

// create controller for grid
app.controller('LifeDataController', ['$scope','LifeService',
    function ($scope, StudentService) {
        var paginationOptions = {
            pageNumber: 1,
            pageSize: 5,
            sort: null
        };

        // method for refresh button
        $scope.loadData = function () {
            StudentService.getLifeData(0,5)
                .success(function(data){
                    $scope.gridOptions.data = data.content;
                    $scope.gridOptions.totalItems = data.totalElements;
                });
        };

        // invoke method for data
        StudentService.getLifeData(
            paginationOptions.pageNumber,
            paginationOptions.pageSize).success(function(data){
            $scope.gridOptions.data = data.content;
            $scope.gridOptions.totalItems = data.totalElements;
        });

        // grid options
        $scope.gridOptions = {
            paginationPageSizes: [5, 10, 20],
            paginationPageSize: paginationOptions.pageSize,
            enableColumnMenus:false,
            useExternalPagination: true,
            columnDefs: [
                { name: 'id', width:'80' },
                { name: 'numRows', width:'140' },
                { name: 'numCols', width:'140' },
                { name: 'rowsJsonData'}
            ],
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.pagination.on.paginationChanged(
                    $scope,
                    function (newPage, pageSize) {
                        paginationOptions.pageNumber = newPage;
                        paginationOptions.pageSize = pageSize;
                        StudentService.getLifeData(newPage,pageSize)
                            .success(function(data){
                                $scope.gridOptions.data = data.content;
                                $scope.gridOptions.totalItems = data.totalElements;
                            });
                    });
            }
        };
    }]);

// service for requests to server for data
app.service('LifeService',['$http', function ($http) {

    function getLifeData(pageNumber,size) {
        pageNumber = pageNumber > 0?pageNumber - 1:0;
        return $http({
            method: 'GET',
            url: 'life/data?page='+pageNumber+'&size='+size
        });
    }
    return {
        getLifeData: getLifeData
    };
}]);