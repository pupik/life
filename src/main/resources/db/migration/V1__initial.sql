CREATE TABLE public.grid_data (
  id           BIGINT AUTO_INCREMENT PRIMARY KEY,
  num_rows     INTEGER,
  num_cols     INTEGER,
  rows_json_data   VARCHAR,
);

CREATE SEQUENCE public.hibernate_sequence;