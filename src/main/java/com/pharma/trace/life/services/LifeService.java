package com.pharma.trace.life.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pharma.trace.life.entities.GridData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface LifeService {

    /**
     * Main method to process current state from the game
     *
     * @param input {@link GridData} that represents the current step of the game
     * @return GridData object{@link GridData} that represents next step of the game
     */
    GridData process(GridData input) throws JsonProcessingException;

    /**
     * Method to get saved data
     *
     * @param pageable {@link Pageable}
     * @return Pageable object with list of gridData{@link GridData} objects
     */
    Page<GridData> getLifeData(Pageable pageable);
}
