package com.pharma.trace.life.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pharma.trace.life.entities.GridData;
import com.pharma.trace.life.repositories.GridDataRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class LifeServiceImpl implements LifeService {

    @Autowired
    private GridDataRepository gridDataRepository;

    @Autowired
    private ObjectMapper mapper;

    private static final int FIELD_SIZE = 20;

    /**
     * {@inheritDoc}
     */
    @Override
    public GridData process(GridData input) throws JsonProcessingException {

        input.setRowsJsonData(mapper.writeValueAsString(input.getRows()));
        gridDataRepository.save(input);
        log.info("current state {}", input.getRowsJsonData());

        GridData output = nextRound(input);

        return output;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Page<GridData> getLifeData(Pageable pageable) {
        return gridDataRepository.findAll(pageable);
    }

    /**
     * Method to count next step of the Game
     * Basic rules
     * 1) If there are three cells around the empty cell - the cell become alive
     * 2) If living cell has two or three neighbours - it stays alive.
     * 3) If living cell has less than two or more than three neighbours - it dies
     *
     * @param current
     * @return
     */
    protected GridData nextRound(GridData current) {
        int numRows = current.getNumRows();
        int numCols = current.getNumCols();
        GridData next = new GridData(numRows, numCols);

        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                int count = countNeighbours(current, i, j);
                boolean isCellAlive = current.getCellValue(i, j);
                // if are 3 live neighbors around empty cells - the cell becomes alive
                isCellAlive = (count == 3) ? true : isCellAlive;
                // if cell has less than 2 or greater than 3 neighbors - it will be die
                isCellAlive = ((count < 2) || (count > 3)) ? false : isCellAlive;
                next.setCellValue(i, j, isCellAlive);
            }
        }
        return next;
    }

    /**
     * Method to count living neighbours around cell
     * Each cell has eight neighbours in different states(living or empty(died))
     *
     * @param current
     * @param x
     * @param y
     * @return
     */
    protected int countNeighbours(GridData current, int x, int y) {
        int count = 0;
        for (int dx = -1; dx < 2; dx++) {
            for (int dy = -1; dy < 2; dy++) {
                int nX = x + dx;
                int nY = y + dy;
                nX = (nX < 0) ? FIELD_SIZE - 1 : nX;
                nY = (nY < 0) ? FIELD_SIZE - 1 : nY;
                nX = (nX > FIELD_SIZE - 1) ? 0 : nX;
                nY = (nY > FIELD_SIZE - 1) ? 0 : nY;
                count += (current.getCellValue(nX, nY)) ? 1 : 0;
            }
        }
        if (current.getCellValue(x, y)) {
            count--;
        }
        return count;
    }

}
