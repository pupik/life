package com.pharma.trace.life.repositories;

import com.pharma.trace.life.entities.GridData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GridDataRepository extends JpaRepository<GridData,Long> {
}
