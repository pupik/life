package com.pharma.trace.life.entities;

import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
@NoArgsConstructor
@Table(name = "grid_data", schema = "public")
public class GridData {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private int numRows;

    @Column
    private int numCols;

    @Column
    private String rowsJsonData;

    @Transient
    private ArrayList<ArrayList<Boolean>> rows;

    public GridData(int numRows, int numCols) {
        this.numRows = numRows;
        this.numCols = numCols;
        this.rows = new ArrayList<>();
        for (int i = 0; i < numRows; i++) {
            ArrayList<Boolean> blankRow = new ArrayList<>();
            for (int j = 0; j < numCols; j++) {
                blankRow.add(false);
            }
            this.rows.add(blankRow);
        }
    }

    public int getNumRows() {
        return numRows;
    }

    public void setNumRows(int numRows) {
        this.numRows = numRows;
    }

    public ArrayList<ArrayList<Boolean>> getRows() {
        return rows;
    }

    public void setRows(ArrayList<ArrayList<Boolean>> rows) {
        this.rows = rows;
    }

    public int getNumCols() {
        return numCols;
    }

    public void setNumCols(int numCols) {
        this.numCols = numCols;
    }

    public boolean getCellValue(int row, int col) {
        return this.rows.get(row).get(col);
    }

    public void setCellValue(int row, int col, boolean val) {
        this.rows.get(row).set(col, val);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRowsJsonData() {
        return rowsJsonData;
    }

    public void setRowsJsonData(String rowsJsonData) {
        this.rowsJsonData = rowsJsonData;
    }

    @Override
    public String toString() {
        return "GridData{" +
                "numRows=" + numRows +
                ", numCols=" + numCols +
                ", rows=" + rows +
                '}';
    }
}
