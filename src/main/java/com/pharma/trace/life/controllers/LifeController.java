package com.pharma.trace.life.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pharma.trace.life.entities.GridData;
import com.pharma.trace.life.services.LifeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller
public class LifeController {

    @Autowired
    private LifeService lifeService;

    @RequestMapping(value = "/life", method = RequestMethod.GET)
    public String lifeView() {
        return "life.html";
    }

    @ResponseBody
    @RequestMapping(value = "/life/process", method = RequestMethod.POST)
    public GridData process(@RequestBody GridData input) throws JsonProcessingException {
        return lifeService.process(input);
    }

    @ResponseBody
    @RequestMapping(value = "/life/data",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<GridData> list(Pageable pageable) {
        return lifeService.getLifeData(pageable);
    }
}
