package com.pharma.trace.life.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pharma.trace.life.entities.GridData;
import com.pharma.trace.life.repositories.GridDataRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class LifeServiceImplTest {

    @InjectMocks
    private LifeServiceImpl lifeService;

    @Mock
    private ObjectMapper objectMapper;

    @Mock
    private GridDataRepository gridDataRepository;

    @Test
    public void process_shouldProcessOk() throws JsonProcessingException {
        GridData input = new GridData();
        input.setRows(new ArrayList<>());
        input.setNumCols(1);
        input.setNumRows(0);

        Mockito.when(objectMapper.writeValueAsString(input.getRows())).thenReturn("rowJsonData");

        GridData result = lifeService.process(input);

        Mockito.verify(gridDataRepository).save(input);
        assertEquals(0, result.getNumRows());
        assertEquals(1, result.getNumCols());
    }

    @Test
    public void getLifeData_shouldProcesssOk() {
        Pageable pageRequest = PageRequest.of(1,1);
        lifeService.getLifeData(pageRequest);
        Mockito.verify(gridDataRepository).findAll(pageRequest);
    }

    @Test
    public void nextRound_theCellShouldStayAlive() {
        GridData input = new GridData(20,20);
        input.setCellValue(4,5,Boolean.TRUE);
        input.setCellValue(4,6,Boolean.TRUE);
        input.setCellValue(4,7,Boolean.TRUE);

        // when two living cells around the cell
        GridData result = lifeService.nextRound(input);
        assertEquals(Boolean.FALSE, result.getCellValue(4,5));
        assertEquals(Boolean.TRUE, result.getCellValue(4,6));
        assertEquals(Boolean.FALSE, result.getCellValue(4,7));

        input.setCellValue(3,6,Boolean.TRUE);
        input.setCellValue(2,6,Boolean.TRUE);

        // when three living cells around the cell
        result = lifeService.nextRound(input);
        assertEquals(Boolean.TRUE, result.getCellValue(4,5));
        assertEquals(Boolean.TRUE, result.getCellValue(4,6));
        assertEquals(Boolean.TRUE, result.getCellValue(4,7));
    }

    @Test
    public void nextRound_theCellShouldBecomeAlive() {
        GridData input = new GridData(20,20);
        input.setCellValue(3,6,Boolean.TRUE);
        input.setCellValue(4,5,Boolean.TRUE);
        input.setCellValue(4,6,Boolean.FALSE);
        input.setCellValue(4,7,Boolean.TRUE);

        GridData result = lifeService.nextRound(input);
        assertEquals(Boolean.TRUE, result.getCellValue(4,6));
    }

    @Test
    public void nextRound_theCellShouldDie() {
        GridData input = new GridData(20,20);
        input.setCellValue(3,6,Boolean.TRUE);
        input.setCellValue(4,5,Boolean.TRUE);
        input.setCellValue(4,6,Boolean.TRUE);
        input.setCellValue(4,7,Boolean.TRUE);
        input.setCellValue(5,6,Boolean.TRUE);

        // when four living cells around the cell
        GridData result = lifeService.nextRound(input);
        assertEquals(Boolean.FALSE, result.getCellValue(4,6));

        // when only one living cell around the cell
        input.setCellValue(4,8,Boolean.TRUE);
        result = lifeService.nextRound(input);
        assertEquals(Boolean.FALSE, result.getCellValue(4,8));

        // when zero living cell around the cell
        input.setCellValue(4,7,Boolean.FALSE);
        result = lifeService.nextRound(input);
        assertEquals(Boolean.FALSE, result.getCellValue(4,8));
    }

    @Test
    public void countNeighbours_shouldReturn2() {
        GridData input = new GridData(20,20);
        input.setCellValue(4,5,Boolean.TRUE);
        input.setCellValue(4,7,Boolean.TRUE);
        int result = lifeService.countNeighbours(input,4,6);

        assertEquals(2,result);
    }

    @Test
    public void countNeighbours_shouldReturn3() {
        GridData input = new GridData(20,20);
        input.setCellValue(4,5,Boolean.TRUE);
        input.setCellValue(4,7,Boolean.TRUE);
        input.setCellValue(3,6,Boolean.TRUE);
        int result = lifeService.countNeighbours(input,4,6);

        assertEquals(3,result);
    }

    @Test
    public void countNeighbours_shouldReturnLessThan2() {
        GridData input = new GridData(20,20);
        input.setCellValue(4,5,Boolean.TRUE);
        int result = lifeService.countNeighbours(input,4,6);

        assertEquals(1,result);
    }

    @Test
    public void countNeighbours_shouldReturnMoreThan3() {
        GridData input = new GridData(20,20);
        input.setCellValue(4,5,Boolean.TRUE);
        input.setCellValue(4,7,Boolean.TRUE);
        input.setCellValue(3,6,Boolean.TRUE);
        input.setCellValue(3,7,Boolean.TRUE);
        int result = lifeService.countNeighbours(input,4,6);

        assertEquals(4,result);
    }
}